package ar.com.osde.dataflowcc.pipeline;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.jdbc.JdbcIO;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.Validation.Required;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CuentaCorrientePipeline {

  private static final Logger LOG = LoggerFactory.getLogger(CuentaCorrientePipeline.class);
  private static final String INSERT_SQL = "INSERT INTO cabecera (codigo_prestador) VALUES(?)";
  private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  private static final String JDBC_URL = "jdbc:mysql://google/dataflow_db?cloudSqlInstance=os-desa-dataflowpoc:us-central1:dataflow-instance&socketFactory=com.google.cloud.sql.mysql.SocketFactory&useSSL=false&user=root&password=root";

  public static class ParseAndConvertToKV extends SimpleFunction<String, KV<String, String>> {

    private static final long serialVersionUID = 1L;

    @Override
    public KV<String, String> apply(String input) {
      String[] split = input.split(" ", 2);
      String key = split[0];
      String value = split[1];
      return KV.of(key, value.substring(0, 16));
    }
  }

  static class CuentaCorrientePreparedStatementSetter implements JdbcIO.PreparedStatementSetter<KV<String,String>> {
    private static final long serialVersionUID = 1L;

    @Override
    public void setParameters(KV<String, String> element, PreparedStatement query) throws SQLException {
      LOG.info("CODIGO PRESTADOR INSERTADO: " + element.getValue());
      query.setString(1, element.getValue());
    }
  }

  public interface CuentaCorrienteOptions extends PipelineOptions {

    @Description("Path of the file to read from")
    String getInputFile();

    @Description("Path of the file to write to")
    @Required
    String getOutput();

    void setOutput(String value);

    void setInputFile(String value);
  }

  public static void main(String[] args) {

    CuentaCorrienteOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
        .as(CuentaCorrienteOptions.class);
    runCuentaCorrientePipeline(options);
  }
  
  static void runCuentaCorrientePipeline(CuentaCorrienteOptions options) {

    Pipeline pipeline = Pipeline.create(options);

    pipeline.apply("ReadLines", TextIO.read().from(options.getInputFile()))

        .apply("ParseAndConvertToKV", MapElements.via(new ParseAndConvertToKV()))

        .apply("InsertRowOnDatabase", JdbcIO.<KV<String, String>>write()
            .withDataSourceConfiguration(JdbcIO.DataSourceConfiguration.create(JDBC_DRIVER, JDBC_URL))
            .withStatement(INSERT_SQL)
            .withPreparedStatementSetter(new CuentaCorrientePreparedStatementSetter()));

    pipeline.run().waitUntilFinish();
  }

}
